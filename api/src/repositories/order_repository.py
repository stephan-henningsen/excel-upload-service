from .generic_repository import GenericRepository


class OrderRepository(GenericRepository):
    def __init__(self, db):
        super().__init__(db, 'orders')


