import pymongo

class RepositoryClient:
    def __init__(self, host, port, username, password, dbname):
        self.client = pymongo.MongoClient(f"mongodb://{username}:{password}@{host}:{port}")
        self.db = self.client[dbname]
