# https://rubikscode.net/2019/11/04/using-mongodb-in-python/
# https://pymongo.readthedocs.io/en/stable/examples/authentication.html
# https://flask-pymongo.readthedocs.io/en/latest/

from bson.objectid import ObjectId


class GenericRepository:
    def __init__(self, db, collection):
        self.collection = db[collection]

    def count(self):
        return self.collection.count()

    def insert(self, document):
        result = self.collection.insert_one(document)
        if result.acknowledged:
            return result.inserted_id
        else:
            return None

    def insert_all(self, documents):
        result = self.collection.insert_many(documents)

    def find_all_ids(self):
        ids = [document["_id"] for document in self.collection.find()]
        return ids

    def find_by_id(self, document_id):
        result = self.collection.find_one({'_id': document_id})
        return result

    def find_by_id_str(self, document_id_str):
        document_id = ObjectId(document_id_str)
        result = self.find_by_id(document_id)
        return result

    def update(self, document):
        result = self.collection.update({"_id": document["_id"]}, document)
        if result["updatedExisting"] == 1:
            return document
        else:
            raise LookupError
