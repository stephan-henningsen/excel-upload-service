from .generic_repository import GenericRepository
import random


class FortuneCookieRepository(GenericRepository):
    def __init__(self, db):
        super().__init__(db, 'fortune-cookies')

    def find_random(self):
        last = self.collection.count()
        r = random.randint(0, max(0,last-1))
        result = self.collection.find().skip(r).limit(1).next()
        return result

