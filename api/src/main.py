#!/usr/bin/env python

from flask import Flask, request, redirect, jsonify, render_template, Response
#import pandas


app = Flask(__name__)
app.config["UPLOAD_FOLDER"] = "/tmp/"
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024 # 16 MB

@app.route("/", methods=["GET"])
def index():
    return "Hi, there!  I'm your API today =)"


@app.route("/catalog/", methods=["POST"])
def post_catalog_form():
	# https://www.askpython.com/python-modules/flask/flask-redirect-url
	customer_id = request.form["customer-id"]
	return redirect("/catalog/" + customer_id + "/", code=307) # Redirect and re-post


@app.route("/catalog/<customer_id>/", methods=["POST"])
def post_catalog(customer_id):
	# https://flask.palletsprojects.com/en/0.12.x/patterns/fileuploads/
	# https://izmailoff.github.io/web/flask-file-streaming/
	file = request.files["file"]

	#doc = pandas.read_excel(file)
	#return doc.to_html()
	#json = to_json(file)
	return "UPLOAD, file.filename=" + file.filename + ", customer-id=" + customer_id


#def to_json(file):
#	doc = pandas.read_excel(file)
	# https://pythonbasics.org/read-excel/




if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True)
